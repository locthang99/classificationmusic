import json
import numpy as np
from sklearn.model_selection import train_test_split
import tensorflow.keras as keras
import matplotlib.pyplot as plt
from keras.models import load_model
import os
import math
import librosa
from flask import Flask, render_template, request
import subprocess
import time
import datetime

SAMPLE_RATE = 22050
TRACK_DURATION = 30  # measured in seconds
SAMPLES_PER_TRACK = SAMPLE_RATE * TRACK_DURATION

#ROOT_INPUT = "/var/www/BBB_Backend/wwwroot/Audio"
ROOT_INPUT = "Input"
PATH_MODEL = "Model/model_40k_Res50_3.h5"

def save_mfcc(file_path, num_mfcc=13, n_fft=2048, hop_length=512, num_segments=10):
    data = {
        "mapping": [],
        "labels": [],
        "mfcc": []
    }
    samples_per_segment = int(SAMPLES_PER_TRACK / num_segments)
    num_mfcc_vectors_per_segment = math.ceil(samples_per_segment / hop_length)
# load audio file
    signal, sample_rate = librosa.load(file_path, sr=SAMPLE_RATE)
    # process all segments of audio file
    for d in range(num_segments):
        start = samples_per_segment * d
        finish = start + samples_per_segment
        mfcc = librosa.feature.mfcc(
            signal[start:finish], sample_rate, n_mfcc=num_mfcc, n_fft=n_fft, hop_length=hop_length)
        mfcc = mfcc.T
        if len(mfcc) == num_mfcc_vectors_per_segment:
            data["mfcc"].append(mfcc.tolist())
    # print(np.array(data["mfcc"]))
    x = np.array(data["mfcc"])
    x = x[..., np.newaxis]
    return x

def CUT(name_file):
    file_path = ROOT_INPUT +"\\"+name_file
    output = "Output"+"\\"+name_file
    lineBash = ''
    time = str(subprocess.check_output("ffprobe -i "+file_path +
               " -show_entries format=duration -v quiet -of csv=\"p=0\"", shell=True))
    duration = int(float(time[2:len(time)-5]))
    start = 15
    part = 1
    while start+30 < duration:
        begin_cut = str(datetime.timedelta(0, start))
        end_cut = str(datetime.timedelta(0, start+30))
        # print(output+"_"+str(part)+".wav")
        if not os.path.exists(output+"_"+str(part)+".wav"):
            lineBash = lineBash + ("ffmpeg -i "+file_path+" -ss "+begin_cut +
                                " -to "+end_cut+" "+output+"_"+str(part)+".wav -loglevel error & ")
        start += 30
        part += 1
    os.system(lineBash)
    return part-1


def predict(model, X):
    X = X[np.newaxis, ...]  # array shape (1, 130, 13, 1)
    prediction = model.predict(X)
    return prediction[0]


# initalize our flask app
app = Flask(__name__)
model = load_model(PATH_MODEL)
model_full = load_model("Model/model_80k_3.h5")
model.summary()
mapping = [
    "viet_nam_rap_viet_",
    "viet_nam_cai_luong_",
    "viet_nam_nhac_tre_v_pop_",
    "viet_nam_nhac_trinh_",
    "viet_nam_nhac_tru_tinh_",
    "viet_nam_nhac_thieu_nhi_",
    "viet_nam_nhac_cach_mang_",
    "viet_nam_nhac_dan_ca_que_huong_",
    "viet_nam_nhac_khong_loi_",
]

mappingFull = [
    "viet_nam_rap_viet_",
    "viet_nam_cai_luong_",
    "viet_nam_nhac_tre_v_pop_",
    "viet_nam_nhac_trinh_",
    "viet_nam_nhac_tru_tinh_",
    "viet_nam_nhac_thieu_nhi_",
    "viet_nam_nhac_cach_mang_",
    "viet_nam_nhac_dan_ca_que_huong_",
    "viet_nam_nhac_khong_loi_",
    "au-my-classical-",
    "au-my-folk-",
    "au-my-country-",
    "au-my-rock-",
    "au-my-alternative-",
    "au-my-rap-hip-hop-"

]


@app.route('/')
def index():
    # initModel()
    # render out pre-built HTML file right on the index page
    return render_template("index.html")

@app.route('/test/', methods=['GET'])
def testAPI():
    songId = request.args.get('songId')
    file = request.args.get('file')
    return {'id':songId,'file':file}
@app.route('/predict/', methods=['GET'])
def predictAPI():
    response = []
    name_file = request.args.get('songId')+".mp3"
    path_file = request.args.get('file')
    #name_file = (request.query_string).decode("utf-8")
    if not os.path.exists("Input/"+name_file):
        os.system("curl -s -L {} -o {}".format(path_file,"Input\\"+name_file))

    TotalPart = CUT(name_file)
    MFCCs = []
    for part in range(TotalPart):
        MFCCs.append(save_mfcc("Output"+"/"+name_file+"_"+str(part+1)+".wav"))
    
    _part = 0
    for mfcc in MFCCs:
        rs = predict(model, mfcc[0])
        res = {}
        for idx, val in enumerate(rs):
            res[mapping[idx]] = val*100

        obj = {'time':str(_part*30+15)+"--"+str(_part*30+45),'predict':res}
        _part+=1
        response.append(obj)
    return {"data":response}

@app.route('/predictFull/', methods=['GET'])
def predictFullAPI():
    response = []
    name_file = request.args.get('songId')+".mp3"
    path_file = request.args.get('file')
    #name_file = (request.query_string).decode("utf-8")
    if not os.path.exists("Input/"+name_file):
        os.system("curl -s -L {} -o {}".format(path_file,"Input\\"+name_file))

    TotalPart = CUT(name_file)
    MFCCs = []
    for part in range(TotalPart):
        MFCCs.append(save_mfcc("Output"+"/"+name_file+"_"+str(part+1)+".wav"))
    
    _part = 0
    for mfcc in MFCCs:
        rs = predict(model_full, mfcc[0])
        res = {}
        for idx, val in enumerate(rs):
            res[mappingFull[idx]] = val*100

        obj = {'time':str(_part*30+15)+"--"+str(_part*30+45),'predict':res}
        _part+=1
        response.append(obj)
    return {"data":response}


if __name__ == "__main__":
    port = int(os.environ.get('PORT', 8089))
    app.run(host='0.0.0.0', port=port)
